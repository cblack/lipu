// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

// QML types
interface Component {};
interface Transition {};

type ParamValue = string | number;

type CallbackAction = "abort" | string | {error: any} | undefined;

type Middleware = (from: Route, to: Route, cont: (action: CallbackAction) => void) => void;

type Signal<T> = Function;

// this would be a QML type, probably just "route"
interface RouteConfig {
    // The path that this route matches
    //
    // Params are supported in the form of /route/:param_name.
    path: string

    // The component that will be instantiated inside an unnamed RouterView {}
    component: Component

    // Named instantiations of a route, e.g. RouterView { name: "name" }
    components?: { [name: string]: Component }

    // Shorthand name, used with `Router.push("name", "params")`
    name?: string

    // default property; Nested routes that will be instantiated inside nested RouterViews {}
    /*

RouterView {
    parent route will be instantiated here!

    RouterView {
        the nested route will be instantiated here when matched!

        RouterView {
            if the nested route has a matched nested route of its own, it'll be instantiated here!
        }
    }
}

    */
    children?: RouteConfig[]

    // associated metadata with a route's configuration
    metadata?: any

    // incoming middlewares
    incomingMiddlewares: Middleware[];
    // outgoing middlewares
    outgoingMiddlewares: Middleware[];
}

// just typeinfo for JS objects
interface Route {
    // the full, absolute path
    path: string

    // the params of the route.
    params: { [name: string]: ParamValue }

    // the routeconfigs passed to the router that match to the current route + its nested ones,
    // in root to leaf order.
    matched: RouteConfig[]
}

// this would be a QML type, which doesn't necessarily have to be root object
interface Router {
    // the current route that this router is displaying
    currentRoute: Route

    // routes known to the router
    // default property
    routes: RouteConfig[]

    // the current URL of the navigator
    url: string

    // parent router, for nested routers
    parent?: Router

    // incoming middlewares
    incomingMiddlewares?: Middleware[]

    // outgoing middlewares
    outgoingMiddlewares?: Middleware[]

    // error handler:
    error?: Signal<{error: any}>

    // push a route onto history stack
    push(url: string): Promise<any>

    // replace the route on top of the history stack
    replace(url: string): Promise<any>

    // pop the route from history, going to the previous entry
    // on system "back" this will always be called with "undefined"
    pop(retValue: any | undefined): void

    // transitions to apply to items being added to and removed from the RouterViews {}
    transitions?: {
        enter?: Transition
        exit?: Transition
        replace?: Transition
    }
}