#include "routertree.hpp"
#include "router.hpp"

#include <QQmlApplicationEngine>
#include <QGuiApplication>

int main(int argc, char *argv[]) {
    QGuiApplication app(argc, argv);

    qmlRegisterType<Router>("org.kde.lipu", 1, 0, "Router");
    qmlRegisterType<Route>("org.kde.lipu", 1, 0, "Route");

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(
        &engine, &QQmlApplicationEngine::objectCreated,
        &app, [url](QObject *obj, const QUrl &objUrl) {
            if ((obj == nullptr) && url == objUrl)
            {
                QCoreApplication::exit(-1);
            }
        },
        Qt::QueuedConnection);
    engine.load(url);

    return QGuiApplication::exec();
}
