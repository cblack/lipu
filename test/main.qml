import org.kde.kirigami 2.12
import org.kde.lipu 1.0
import QtQuick 2.10

ApplicationWindow {
    Router {
        Route {
            path: "/users/:user_id"

            Route {
                path: "/delete"
            }
        }
    }

    Component.onCompleted: print($router)
}