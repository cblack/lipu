#pragma once

#include "router.hpp"
#include "routertree.hpp"

struct Router::Private {
    QList<QPointer<Route>> routes;
    RouterTree<QPointer<Route>> routeTree;
    QString location;
    QJsonValue parameters;
};