QtApplication {
	name: "test"

	cpp.cxxLanguageVersion: "c++20"
	cpp.cppFlags: ['-Werror=return-type']

	files: [
		"*.cpp",
		"*.hpp",
		"test.qrc",
	]

	Depends { name: "Qt"; submodules: ["qml", "quick", "gui"] }
}
