// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

import Lipu.Router 1.0
import org.kde.kirigami 2.10 as Kirigami

Item {
    RouterView {
        // the kirigami page would be instantiated in this outer view
    }

    Router {
        Route {
            path: "/users/:user_id"

            Kirigami.Page {
                title: State.usernameForUser($router.params.user_id)

                Button {
                    // require absolute paths instead of trying to figure out where
                    // a relative path should go to increase robustness
                    //
                    // some diffing shenanigans would be used to reduce waste
                    onClicked: $router.push(`/users/${$router.params.user_id}/manage`).then(value => {

                        // this would print ok or not ok depending on which button was used to pop the route
                        console.log("the user clicked: " + value)
                    })
                }

                RouterView {
                    // the kirigami overlaysheet would be instantiated here
                }
            }

            Route {
                path: "/manage"

                Kirigami.OverlaySheet {
                    Button {
                        onClicked: $router.pop("ok")
                    }
                    Button {
                        onClicked: $router.pop("not ok")
                    }
                }
            }
        }
    }
}